package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
//This application will function as an endpoint that will used in handling http request
@RestController
//Will require all routes within the class to use the set endpoint as part of its route.
@RequestMapping("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);}

	// localhost:8080/hello
	@GetMapping("/hello")
	// Maps a get request to the route "/hello" and invokes the method hello()
	//every changes do ctrl + c to type in terminal and ./mvnw spring-boot:run
	public String hello(){
		return "Hello World";
	}
	// Route with String query
	//http://localhost:8080/hi?name=Tolits
	@GetMapping("/hi")
	// RequestParam annotation that allows us to extract data from query String in the url
	public String hi(@RequestParam(value="name", defaultValue="John")String name){
		return String.format("Hi %s",name);
	}
	//Multiple parameters
	//localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name",defaultValue="Joe")String name, @RequestParam(value="friend",defaultValue = "jane") String friend){
		return String.format("Hello %s! My name is %s",friend,name);
	}

	//route with path variables
	//dynamic data is obtained directly from the url
	//localhost:8080/{name}
	@GetMapping("/hello/{name}")
	// @pathvariable annotation allows us to extract data directly from the url
	public String greetFriend(@PathVariable("name")String name){
		return String.format("Nice to meet you %s!",name);
	}

	//Activity for s09
		// 1.
	private List<String> enrollees = new ArrayList<>();
		// 2.
	@GetMapping("/enroll")
	public String enrollStudent(@RequestParam("user") String user) {
		enrollees.add(user);
		return String.format("Thank you for enrolling, %s!",user);
	}
		// 3.
	@GetMapping("/getEnrollees")
	public List<String> getEnrollees() {
		return enrollees;
	}
		//4.
	@GetMapping("/nameage")
	public String nameage(@RequestParam(value="name",defaultValue="Joe")String name, @RequestParam(value="age",defaultValue = "1") int age){
		return String.format("Hello %s! My age is %d.",name,age);
	}
		//5.
	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id")String id){
		return switch (id) {
			case "java101" -> "Name: Java 101, Schedule: MWF 8:00AM-11:00AM, Price: PHP 3000.00";
			case "sql101" -> "Name: SQL 101, Schedule: TTH 1:00PM-4:00PM, Price: PHP 2000.00";
			case "javaee101" -> "Name: java EE 101, Schedule: MWF 1:00PM-4:00PM, Price: PHP 3500.00";
			default -> "Course cannot be found";
		};
	}


	//Async Activity
	//1. http://localhost:8080/greeting/welcome/?user=Jill&role=admin
	@GetMapping("/welcome/")
	public String userrole(@RequestParam(value="user",defaultValue="Joe")String user, @RequestParam(value="role",defaultValue = "student") String role){
		return switch (role) {
			case "admin" -> String.format("Welcome back to class portal, Admin %s!",user);
			case "teacher" -> String.format("Thank you for logging in, Teacher %s!",user);
			case "student" -> String.format("Welcome to the class portal, %s!",user);
			default -> "Role out of range";
		};
	}
	//2.

	static class Student {
		private int id;
		private String name;
		private String course;

		public Student(int id, String name, String course) {
			this.id = id;
			this.name = name;
			this.course = course;
		}

		public int getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public String getCourse() {
			return course;
		}
	}
	ArrayList<Student> students = new ArrayList<>();
	//localhost:8080/greeting/register?id=123456789&name=John&course=BSIT
	@GetMapping("/register")
	public String registerStudent(@RequestParam(value="id",defaultValue="1") int id, @RequestParam(value="name",defaultValue="Joe") String name, @RequestParam(value="course",defaultValue="BSIT") String course) {
		Student student = new Student(id, name, course);
		students.add(student);
		return id + " your id number is registered on the system!";
	}
	//http://localhost:8080/greeting/account/123456789
	@GetMapping("/account/{id}")
	public String getStudentDetails(@PathVariable("id") int id) {
		for (Student student : students) {
			if (student.getId() == id) {
				return "Welcome back " + student.getName() + "! You are currently enrolled in " + student.getCourse();
			}
		}
		return "Your provided "+id +" is not found in the system!";
	}


}
